package com.ldileh.testdatabinding;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ldileh.testdatabinding.databinding.ListItemBinding;

/**
 * Created by ldileh on 21/07/16.
 */
public class Adapter extends BaseAdapter {
    private Context context;
    private ObservableArrayList<ModelItemList> list;

    public Adapter(Context context, ObservableArrayList<ModelItemList> mItems){
        this.context = context;
        this.list = mItems;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item, null);
        }

        return convertView;
    }

}
