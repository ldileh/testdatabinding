package com.ldileh.testdatabinding;

import android.databinding.ObservableArrayList;

/**
 * Created by ldileh on 21/07/16.
 */
public class ModelMain {
    public ObservableArrayList<ModelItemList> list = new ObservableArrayList<>();

    public ModelMain() {
    }

    public void add(String text, String title) {
        list.add(new ModelItemList(text, title));
    }

}
