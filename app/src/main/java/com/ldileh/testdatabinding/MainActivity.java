package com.ldileh.testdatabinding;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.ldileh.testdatabinding.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_main);
        ModelMain modelMain = new ModelMain();
        binding.setModel(modelMain);

        for (int i = 0; i < 5; i++) {
            modelMain.add("text " + Integer.toString(i), " title " + Integer.toString(i));
        }
    }
}
