package com.ldileh.testdatabinding;

import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.widget.ListView;

/**
 * Created by ldileh on 21/07/16.
 */
public class Binder {
    @BindingAdapter("items")
    public static void bindListKomentar(ListView view, ObservableArrayList<ModelItemList> list) {
        AdapterList adapter = new AdapterList(view.getContext(), list);
        view.setAdapter(adapter);
    }
}
