package com.ldileh.testdatabinding;

/**
 * Created by ldileh on 21/07/16.
 */
public class ModelItemList {
    String text;
    String title;

    public ModelItemList(String text, String title){
        this.text = text;

        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }
}
