package com.ldileh.testdatabinding;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ldileh.testdatabinding.databinding.ListItemBinding;

import java.util.List;

/**
 * Created by ldileh on 21/07/16.
 */
public class AdapterList extends BaseAdapter {
    private Context context;
    private ObservableArrayList<ModelItemList> list;
    private LayoutInflater inflater;

    public AdapterList(Context context, ObservableArrayList<ModelItemList> mItems){
        this.context = context;
        this.list = mItems;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        ListItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.list_item, parent, false);
        binding.setModel(list.get(position));

        return binding.getRoot();
    }

}
